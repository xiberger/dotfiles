#!/bin/sh
xrandr --output DVI-D-0 --off --output HDMI-0 --mode 1680x1050 --pos 2560x0 --rotate right --output DP-5 --off --output DP-4 --off --output DP-3 --off --output DP-2 --mode 2560x1440 --pos 0x240 --rotate normal --output DP-1 --off --output DP-0 --off
