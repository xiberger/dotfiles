# some required Packages
+ i3 group package
+ lightdm (if no displaymanger is installed)
+ dmenu as app launcher (before ricing)
+ feh (for background)
+ lxappearance-gtk3 (for changing style of windows)
+ rofi (pacman -S rofi) alternative for dmenu
+ compton (window compositor)
+ dunst (notificaton dameon)
+ lxrandr, arandr for display/monitor management
+ awesome-terminal-fonts powerline ttf-font-awesome 

# Configuration Files
link `config/i3/config` 

# find Window class in xorg
use package `xorg-xprop` for usage in the `config`s `for_window` sections

# Background 
the program `feh` is used for setting the background, it is also a small picture viewer
see the `feh` line in config to change background

# Keyboard-Layout
already in config 
todo remove capslock key

# PulseAudio Volume-Key settings
already in config 

# lxappearance - changes GTK themes
Use whatever GTK theme which you like. Might need to change config files in `.config/gtk3*` as well es in `~.gtk2*`
Installed on t440p:
+ Theme: Adapta-Nokto-Eta
+ IconTheme: archdroid-icon-theme
+ Font: Noto Sans Light 13pt
+ font for statusbar same Noto Sans Light 13pt
+ font for window title Noto Sans Light 10pt

# Display/ Output Management
use `lxrandr` or `arandr` for a ui for thta
in `config/i3/dualMonitor.sh` dual

# Programlauncher rofi
added to config - with current configuration it only works with .desktop files 

# Notification Daemon (todo)
dunst

# Screenlock (todo)

## Backlight settings (fixme)
Steps done on T440p which resultet to a somewhat working config:
+ Add `acpi_backlight=video` to the `options` in `/boot/loader/entries/arch.conf`
+ add `backlight.rules` to `/etc/udev/rules.d` with following two rules:
	- `ACTION=="add", SUBSYSTEM=="backlight", KERNEL=="acpi_video0", RUN+="/bin/chgrp video /sys/class/backlight/%k/brightness"`
	- `ACTION=="add", SUBSYSTEM=="backlight", KERNEL=="acpi_video0", RUN+="/bin/chmod g+w /sys/class/backlight/%k/brightness"`
+ TODO who is controlling this xbacklight is not controlling it... (with gnome pre installed)
+ TODO how to increase steps?

# Network utility 
`NetworkManager` to change networks

# bluetooth utility 
`Blueman` - has some dbus issues, but works (for now)

## smaller file manager (todo)
## keychain? (todo)
example for 

# Statusbar
## Polybar
for notebook remove monitor config line
## i3blocks
todo testing.. 


### Notifications (dunst)

# Source
[LARBS](https://github.com/LukeSmithxyz/LARBS)
[alex booker dotfiles](https://github.com/bookercodes/dotfiles/tree/ubuntu/.i3)
[bumblebee-status](https://github.com/tobi-wan-kenobi/bumblebee-status)
[polybar](https://github.com/jaagr/polybar/wiki) 
