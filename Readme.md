# dotFiles, useful Scripts and other ReadMes
the idea of this repository is to make a global config over all machines

# Installation
to install the config do the following steps

## Prerequisites
* In order to function you have to install zsh via the `oh-my-zsh` github repo
* next install thefuck from aur or elsewhere
* make shure vim is installed
* install the powerline9k theme (next section)

## Install powerlevel9k zsh-Theme
go to [powerlevel9k](https://github.com/bhilburn/powerlevel9k/wiki/Install-Instructions) and install it
install [nerdfonts](https://github.com/ryanoasis/nerd-fonts) to use in the profile

## Installing the config
* clone repo into `$USER/workspace/dotfiles`
* run the `link.sh` script to make all symbolic links to the following files/folders
	* `.vimrc`
	* `.vim`
	* `.zshrc`
	* `.bashrc`
	* `.tmux.conf`
	* `gitconfig`
	* `.config/trizen/trizen.conf`
	* linking of `/usr/share/zsh-theme-powerline9k` to `$HOME/.oh-my-zsh/themes/custom`
* if running GNOME as DM take a look at the `gtk.css` to reduce panel height

# Scripts
Place custom scripts in `path/to/dotfiles/scripts` and make them executable - these are are automagically in the `$PATH`

# Arch Linux Installation Lenovo T440p
If you have problems connecting to your Bluetooth-Headset try the following:
* `mkdir -p ~gdm/.config/systemd/user`
* `ln -s /dev/null ~gdm/.config/systemd/user/pulseaudio.socket`
src: [Manjaro Forum](https://forum.manjaro.org/t/bluetooth-audio-device-connects-but-doesnt-show-in-sound-settings/20613/9)


# todo
* `.vscodeconfig` 
* update vim plugins - remove none needed
* add ssh config?
* ADD global jetbrains shortcuts
* add [GNOME shortcuts](https://unix.stackexchange.com/questions/119432/save-custom-keyboard-shortcuts-in-gnome)

