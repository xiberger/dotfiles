#!/bin/bash
git difftool --tool="diffall" --no-prompt "$@"
meld `cat /home/mike/workspace/dotfiles/git_tools/tmp/diffall 2> /dev/null | tr -d '\n'` > /dev/null 2>&1
rm `cat /home/mike/workspace/dotfiles/git_tools/tmp/diffall_rm 2> /dev/null | tr -d '\n'` > /dev/null 2>&1
rm /home/mike/workspace/dotfiles/git_tools/tmp/diffall  > /dev/null 2>&1
rm /home/mike/workspace/dotfiles/git_tools/tmp/diffall_rm > /dev/null 2>&1
