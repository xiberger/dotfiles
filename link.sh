#!/bin/sh

# src: https://github.com/djfo/dotfiles/blob/master/link.sh
escape() {
	echo -e "\033[;$1m$2\033[0m"
}

err() {
	escape 31 "$1"
}

info() {
	escape 33 "$1"
}

link_dot()  {
	ln -sf `pwd`/$1 $HOME/.$1
}

link_conf() {
	ln -sf `pwd`/$2 $HOME/.config/$1/$2
}

is_installed() {
	which $1 > /dev/null 2> /dev/null
}

# check if oh-my-zsh is installed
ls $HOME/.oh-my-zsh > /dev/null 2> /dev/null

if [ $? -ne 0 ]; then
	err "oh-my-zsh is not installed"
	info "go to https://github.com/robbyrussell/oh-my-zsh"
	exit 1
fi

# powerlevel9k 
ls /usr/share/zsh-theme-powerlevel9k > /dev/null 2> /dev/null

if [ $? -ne 0 ]; then
	# check if already there
	ls $HOME/.oh-my-zsh/custom/themes/powerlevel9k > /dev/null 2> /dev/null
	if [ $? -ne 0 ]; then
		err "powerlevel9k not installed"
		info "go to https://github.com/bhilburn/powerlevel9k/wiki/Install-Instructions to install the theme AND nerdfonts"
		exit 1
	fi
else 
	ln -sf /usr/share/zsh-theme-powerlevel9k $HOME/.oh-my-zsh/custom/themes/powerlevel9k > /dev/null
	info "linked /user/share/zsh-theme-powerlevel9k to ~/.oh-my-zsh/custom/themes/powerlevel9k"
fi


# bashrc
if is_installed bash; then
	link_dot bashrc
	info ".bashrc linked"
else
	err "bash not installed, but why?!"
fi

# zshrc
if is_installed zsh; then
	link_dot zshrc
	info ".zshrc linked"
else
	err "zsh is not installed"
fi

# vim and vimrc
if is_installed vim; then
	link_dot vim
	link_dot vimrc
	unlink `pwd`/vim/vim >/dev/null 2> /dev/null
	info ".vim and .vimrc linked"
else
	err "vim is not installed"
fi

# tmux
if is_installed tmux; then
	link_dot tmux.conf
	info ".tmux.conf linked"
else
	err "tmux not installed"
fi

# gitconfig
if is_installed git; then
	link_dot gitconfig
	info ".gitconfig linked"
	cp gitconfig.local $HOME/.gitconfig.local
	info "copied ~/.gitconfig.local please enter credentials"
else
	err "git is not installed how can you live with yourself?!"
fi

# thefuck
is_installed thefuck 
if [ $? -eq 1 ]; then
	err "thefuck is not installed - go to https://github.com/nvbn/thefuck or install via AUR"
fi

# trizen
if is_installed trizen; then
	link_conf trizen trizen.conf
	info ".conf/trizen/trizen.conf linked"
else
	err "trizen not installed! - go to https://github.com/trizen/trizen"
fi


